Promise.all([
  fetch('./icons.json').then(res => res.json()),
  fetch('./sources.txt').then(res => res.text())
])
.then(([icons, txt]) => {
  const sources = txt.split('\n').reduce((obj, line) => {
    const [id, name] = line.split(" '=' ");
    obj[id.toLowerCase()] = name;
    return obj;
  }, {});
  const container = document.getElementById('sources');
  container.innerHTML = `<table>
    <thead>
      <tr>
        <th>song.ini icon entry</th>
        <th>Icon preview</th>
        <th>Full source name</th>
      </tr>
    </thead>
    <tbody>
      ${icons.map(icon => {
        const id = icon.name.replace('.png', '');
        return `<tr>
          <td align="right">${id}</td>
          <td align="center"><img alt="" src="./icons/${icon.name}" /></td>
          <td>${sources[id.toLowerCase()] || '-'}</td>
        </tr>`;
      }).join('\n')}
    </tbody>
  </table>`;
});
